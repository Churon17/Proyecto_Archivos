﻿
using Proyecto_Archivos.Modelos;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Archivos.Controlador
{
    public class ConProfesor
    {
        #region  caracteristicas


        private string ruta = @"C:\\sistema\\Profesor.txt";

        private string[] letras = new string[6];
        

        #endregion



        #region Metodos


        public bool modificar(string cedula, Profesor profe)
        {


            bool eliminado = false;

            string temporalRuta = @"C:\\sistema\\temp.txt";

            string[] arreglo = new string[6];

            File.CreateText(temporalRuta).Close();

            StreamReader leer = new StreamReader(ruta);

            StreamWriter escribir = new StreamWriter(temporalRuta);

            string linea = leer.ReadLine();
            try
            {

                while (linea != null)
                {
                    arreglo = linea.Split(';');

                    if (!arreglo[3].Equals(cedula))
                    {

                        escribir.WriteLine(linea);

                    }
                    else
                    {

                        String lineaNueva = profe.Nombre + "; " + profe.Apellido + ";" + profe.Edad + ";" + profe.Cedula + ";"
                         + profe.Horas_clase + ";" + profe.Cantidad_cursos + ";";

                        escribir.WriteLine(lineaNueva);
                    }

                    linea = leer.ReadLine();

                }
                eliminado = true;
            }
            catch
            {

            }

            leer.Close();

            escribir.Close();

            File.Delete(ruta);

            File.Move(temporalRuta, ruta);

            return eliminado;







        }



        public bool elimnar(string cedula)
        {
            bool eliminado = false;

            string temporalRuta = @"C:\\sistema\\temp.txt";

            string [] arreglo = new string[6];

            File.CreateText(temporalRuta).Close();
             
            StreamReader leer = new StreamReader(ruta);

            StreamWriter escribir = new StreamWriter(temporalRuta);

            string linea = leer.ReadLine();
            try
            {

                while (linea != null)
                {
                    arreglo = linea.Split(';');

                    if (!arreglo[3].Equals(cedula))
                    {

                        escribir.WriteLine(linea);

                    }

                    linea = leer.ReadLine();

                }
                eliminado = true;
            }
            catch
            {
 
            }

            leer.Close();

            escribir.Close();

            File.Delete(ruta);

            File.Move(temporalRuta, ruta);

            return eliminado;
        }

     

        public bool escribirArchivo(Profesor profe) 
        {

            bool boolEscribir = true;

            StreamWriter writer = new StreamWriter(ruta, true);


            string escribir = profe.Nombre + "; " + profe.Apellido + ";" + profe.Edad + ";" + profe.Cedula + ";"
                + profe.Horas_clase + ";" + profe.Cantidad_cursos + ";";
            try
            {

                writer.WriteLine(escribir);

                
            }
            catch
            {

                boolEscribir = false;

            }

            writer.Close();

            return boolEscribir;




        }

        public  List<Profesor> leerArchivo( )
        {
           
            StreamReader reader = new StreamReader(ruta);

            List<Profesor> lista = null;

            string palabra = reader.ReadLine();


            if(palabra!= null)
            {

                lista = new List<Profesor>();

                while(palabra != null)
                {
               
                    letras = palabra.Split(';');

                    Profesor profesor = new Profesor();

                    profesor.Nombre = letras[0];

                    profesor.Apellido = letras[1];

                    profesor.Edad =   Convert.ToInt32( letras[2]);

                    profesor.Cedula = letras[3];

                    profesor.Horas_clase = Convert.ToInt32(letras[4]);

                    profesor.Cantidad_cursos = letras[5];

                    lista.Add(profesor);
                    
                    palabra = reader.ReadLine();
                    

                }

            }

            reader.Close();
            

            return lista;

        }

        #endregion
    }
}
