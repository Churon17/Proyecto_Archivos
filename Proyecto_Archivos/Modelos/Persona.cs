﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Archivos.Modelos
{
   public class Persona
    {
        private string nombre;

        private string apellido;

        private int edad;

        private string cedula;

        public string Nombre { get => nombre; set => nombre = value; }

        public string Apellido { get => apellido; set => apellido = value; }

        public int Edad { get => edad; set => edad = value; }

        public string Cedula { get => cedula; set => cedula = value; }
    }
}
