﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Archivos.Modelos
{
    public class Profesor : Persona
    {

        private int horas_clase;


        private string cantidad_cursos;

        public int Horas_clase { get => horas_clase; set => horas_clase = value; }
        public string Cantidad_cursos { get => cantidad_cursos; set => cantidad_cursos = value; }
    }
}
