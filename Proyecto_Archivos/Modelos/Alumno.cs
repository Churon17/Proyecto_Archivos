﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Archivos.Modelos
{
    public class Alumno : Persona
    {
        private int cantidad_horas;


        private string nombre_curso;

        public int Cantidad_horas { get => cantidad_horas; set => cantidad_horas = value; }
        public string Nombre_curso { get => nombre_curso; set => nombre_curso = value; }
    }
}
